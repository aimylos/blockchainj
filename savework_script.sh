#!/bin/bash
#general purpose archive script

#current archive directory with time
NAME=${PWD##*/}
DT=$(date '+%Y%m%d%H%M')
ARCHDIRNAME=${NAME}_${DT}
if [ "$#" == 1 ]; then
	ARCHDIRNAME=${ARCHDIRNAME}_$1
fi

function archive {
	ARCHPATH=$1
	mkdir -p ${ARCHPATH}

	#check for same name
	INITDIRNAME=$2
	DIRNAME=$2
	COUNTER=1
	while [ -d "${ARCHPATH}/${DIRNAME}" ]; do
		COUNTER=$((COUNTER + 1))
		DIRNAME=${INITDIRNAME}_${COUNTER}
	done

	mkdir ${ARCHPATH}/${DIRNAME}

	#copy files to archive directory
	cp -r ./* ${ARCHPATH}/${DIRNAME}/
}

#local archives directory
#LOCALARCHPATH=${HOME}/Archives
#archive $LOCALARCHPATH $ARCHDIRNAME

#dropbox archives directory
DROPBOXARCHPATH=${HOME}/Dropbox/Archives
archive $DROPBOXARCHPATH $ARCHDIRNAME