The UTXO Set implementation documented in Section~\ref{utxosetdatastructure} has been implemented and evaluated. The results are presented in this chapter.

\section{Evaluation System \& Parameters} \label{evaluationsystemandparameters}

\paragraph{Blockchainj System} In order to see the UTXO Set data-structure documented in Section~\ref{utxosetdatastructure} in action, a custom-made Java Library, called Blockchainj~\cite{blockchainjrepo}, was built. The Blockchainj Library has purpose-built UTXO Set data-structures that split the UTXO Set into shards and use that information for efficient computation and storage. The Library also provides means of measurement of information about blocks and the UTXO Set, its shards and its Merkle tree.

The system was set up as seen in Figure~\ref{fig:blockchainjsystem}. The properties that the system has are:
\begin{itemize}
	\item The Bitcoin Rules must hold in order to apply a block to the UTXO Set.
	\item The blocks themselves are not stored locally.
	\item The UTXO Set is stored locally, but only its most recent version, except for periodic archives of the UTXO Set which are stored locally too.
	\item The UTXO Set can be accessed as a Set of Shards. The number of shards is fully dynamic and can be any number which is a power of 2.
	\item Measurements are recorded about the blocks, the UTXO Set and the Shards, such as size (bytes), transaction cardinallities ect... 
\end{itemize}

\begin{figure}[t]
\centering
\includegraphics[width=0.8\textwidth]{blockchainjSystem.png}
\caption{Blockchainj System. The system computes the UTXO Set by applying sequential blocks to the UTXO Set. The modifications that a block causes on the UTXO Set when applied are only permanent (commited) if the block is valid and the Bitcoin Protocol rules hold.}
\label{fig:blockchainjsystem}
\end{figure}

\paragraph{Bitcoin's Blockchain} The system of Figure~\ref{fig:blockchainjsystem} will run on the main Bitcoin Blockchain and compute the UTXO Set for a block height of up to \textbf{520,000}. 

It is important to note that while the implementation of Section~\ref{includingmerklerootintoblock} requires that the UTXO Set Merkle tree root be included in the block, this evaluation system will not do so, as it is practically impossible to alter the blocks of the Bitcoin Blockchain, as described in Section~\ref{bitcoinimmutability}. Nevertheless, the UTXO Set Merkle tree is computed and kept up-to-date for every block appended to the Blockchain, which was mined on 2018-04-26.


\paragraph{Parameters}\label{parameters} Based on the documentation of Section~\ref{utxosetdatastructure} the adjustable parameter that has to be examined is the shard size. Since the indexing of transaction outputs into shards is done using the first bits of their TXID, the shard will not be of equal size, because a nonequal amount of transaction outputs of nonequal size are inserted into each shard. Nevertheless, it is expected that the shard size has a small variance, because the TXIDs are SHA256 hashes which have a uniform bit distribution,  therefore the average shard size is a reliable indicator. The shard size variance is measured for completeness.

The average shard size is in direct correlation with the number of shards. It is expected that each time the number of shards doubles, the average shard size is halfed. The number of shards is in turn equal to 2 raised to the power of the number of index bits used to index the transaction outputs' TXID into shards:
\begin{equation}
	\text{number of shards}=2^{\text{index bits}}
\end{equation} 
Therefore, the adjustable parameter for this evaluation will be the number of index bits. More specifically, the following index bits numbers are considered:
\begin{equation}\label{eq:bitsrange}
	\text{index bits} \in \lbrace 0, 1, ..., 26 \rbrace
\end{equation}
\begin{itemize}
	\item The case of 0 index bits means that the UTXO Set is grouped altogether in a single shard.
	
	\item The case of 1 index bit means that the UTXO Set is split into two shards.
	
	\item ...
	
	\item The case of 26 index bits means that the UTXO Set is split into 67,108,864 shards, which corresponds to a UTXO Set Merkle tree of $2^{26} = 2*67,108,864-1$ nodes each of size 256 bits, as explained in Section~\ref{shardsizevsmerkletreesize} and~\ref{merkletreedatastructure}. 
\begin{equation}\label{eq:merkletreesize}
	\text{UTXO Set Merkle tree size} = (2*2^{\text{index bits}} - 1) * \frac{256}{8} \text{ bytes}
\end{equation}	
Therefore, when using 26 index bits the Merkle tree size is 4,294,967,264 bytes or about 4GB. As shown in Figure~\ref{fig:utxosetsize}, the UTXO Set size has reached a size of 3GB, so it makes no sense to examine higher index bits numbers as the UTXO Merkle tree will outgrow the UTXO Set in size dramatically, which in turn would make it more profitible to download the whole Bitcoin Blockchain and build the UTXO Set instead of downloading the UTXO Set and verify it via the UTXO Set Merkle tree. It is reminded that one of the goals of the proposal in Section~\ref{encodingstateintoblock} is to avoid downloading the entire Bitcoin Blockchain.
\end{itemize}
  

\section{UTXO Set size} \label{evaluationutxosetsize}
\paragraph{Measuring the UTXO Set Size} The UTXO Set size has been measured by including the following in its size:
\begin{itemize}
	\item The serialized size of the UTXO Set data-structure and its data, as described in Table~\ref{tab:utxosetformat}.
	\item The UTXO Set Merkle tree data-structure, as described in Table~\ref{tab:merkletreeformat}.
\end{itemize}

The UTXO Set size has been measured for every block height up to 520,000 and for the Shard index bits values of the set~\ref{eq:bitsrange}, presented in Section~\ref{evaluationsystemandparameters}.

\paragraph{UTXO Set Size Measurements} The UTXO Set size measurements can be seen in both logarithmic and linear scale in Figures~\ref{fig:utxosetsizes} and~\ref{fig:utxosetsizeslinear}, respectively. About these Figures:
\begin{itemize}
	\item As expected the UTXO Set size grows larger as the index bits increase. This happens because:
	\begin{itemize}
		\item The UTXO Set Merkle tree doubles in size every time the index bits are incremented by 1.
		\item The accumulated shard metadata (version 4 bytes, index 8 bytes and UTXOs\_count 1 to 9 bytes, in  Table~\ref{tab:shardformat}), are doubled every time the index bits are incremented by 1 because the number of shards is doubled.
\begin{equation}\label{eq:shardmetadataoverhead}
		2^{\text{index bits}} * 13 \leq \text{shard metadata overhead} \leq 2^{\text{index bits}} * 21 \text{ bytes}
	\end{equation}
	\end{itemize}
	
	\item When the index bits value is 0, the UTXO Set size is optimally minimum. This happens because when the index bits are 0 there is only one shard and the Merkle tree consists of only its root.
	
	\item For index bits values up to 19, the UTXO Set size is contained within the order of the optimal minimum size. This happens because the UTXO Set Merkle tree size is only 32MB (from Equation~\ref{eq:merkletreesize}) and the shard metadata overhead is about 7,5MB (from Equation\ref{eq:shardmetadataoverhead}.
	
	\item For index bits values of 25 and 26 the UTXO Set merkle tree size is 2GB and 4GB respectively and the shard metadata overhead is 416MB and 832MB respectively.	
\end{itemize}


\begin{figure}[H]
\centering
\includegraphics[]{utxosetSizes.pdf}
\caption{UTXO Set sizes (bytes) in logarithmic scale. The figure includes graphs of various values for the \textbf{index bits parameter}.}
\label{fig:utxosetsizes}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[]{utxosetSizesLinear.pdf}
\caption{UTXO Set sizes (bytes) in linear scale. The figure includes graphs of various values for the \textbf{index bits parameter}.}
\label{fig:utxosetsizeslinear}
\end{figure}


\section{Average Shard Size} \label{evaluationaverageshardsize}
\paragraph{Measuring the Average Shard Size} The average shard size is calculated as follows:
\begin{equation}
	\text{average shard size} = \frac{\sum_{0}^{2^{\text{index bits}}-1} \text{shard}_i\text{ size}}{2^{\text{index bits}}}
\end{equation}
The serialized size of a shard, say $\text{shard}_i$, is computed from Table~\ref{tab:shardformat}.

Again, the shard size has been measured for every UTXO Set of block heights up to 520,000 and for the shard index bits values of the set~\ref{eq:bitsrange}.

\paragraph{Shard Size Measurements} The shard size measurements can be seen in Figure~\ref{fig:avgshardsize}. About the Figure:
\begin{itemize}
	\item The average shard size is, as expected, halved every time the index bits value is incremented by 1.
	
	\item When the index bits value is 0, the average shard size is the same as the UTXO Set size. This happens because when the index bits value is 0 the UTXO Set consists of only one shard.
	
	\item The minimum shard size is achieved when the index bits take their maximum value, which is 26 for this evaluation. The average shard size is just about 64 bytes when the index bits take the value 26.
\end{itemize}

\begin{figure}[t]
\centering
\includegraphics[]{shardAvgSize.pdf}
\caption{Average shard size. The figure includes graphs of various values for the \textbf{index bits parameter}.}
\label{fig:avgshardsize}
\end{figure}


\paragraph{Shard Size Standard Deviation} The average shard sizes in Figure~\ref{fig:avgshardsize} are not surprising, as they follow the pattern predicted in Section~\ref{shardsizevsmerkletreesize}. In fact, the average shard size can also be directly calculated by dividing the UTXO Set size, without including the Merkle tree size, by the number of shards:
\begin{equation}
	\text{average shard size} = \frac{\text{UTXO Set size (Merkle tree size }\textbf{not included)}}{2^{\text{index bits}}}
\end{equation}

What is actually interesting to find out is, how good is the average shard size indicator. The shard size distribution should be uniform, as mentioned in Section~\ref{parameters}. In order to examine the shard size distribution more closely, the standard deviation of the shard size has been calculated and can be seen in Figure~\ref{fig:varshardsize}. It is noted that the (uncorrected sample) standard deviation $s$ is calculated as follows:
\begin{equation}
	s = \sqrt{ \frac{ \sum_{0}^{2^{\text{index bits}}} (\text{shard}_i\text{ size} - \text{average shard size})^2 }{2^{\text{index bits}}} }
\end{equation}
About the standard deviation for the shard size in Figure~\ref{fig:varshardsize}:
\begin{itemize}
	\item For index bits values of 1 through 4, the standard deviation is within the 16MB or less. This deviation is to be expected since the shard sizes are within the orders of hundreds of MB as shown in Figure~\ref{fig:avgshardsize}.
	
	\item For index bits values of 8 through 19, the standard deviation is counted in KB.
	
	\item For index bits values of 25 and 26, the standard deviation is less than 1 KB.
\end{itemize}
Overall, the standard deviation between the shard sizes is considerably low, which confirms the assumption that the shard size follows a uniform distribution. This means that the Unspent Transaction Ouputs are being assigned uniformly to their corresponding shards and the average shard size is a good indicator for the shard size itself.

\begin{figure}[t]
\centering
\includegraphics[]{shardVarSize.pdf}
\caption{Standard deviation of the shard size, between shards of the same bit index. The figure includes graphs of various values for the \textbf{index bits parameter}.}
\label{fig:varshardsize}
\end{figure}



\section{Block Verification Bandwidth} \label{evaluationblockverificationbandwidth}

\paragraph{Block Verification} This measurement concerns the Bitcoin user that does not have the UTXO Set stored locally. So in order to validate a block, the user has to download the necessary shards that contain the Unspent Transaction Outputs that have been ``spent'' by the block and the necessary shards in which the new Unspent Transaction Outputs created by the block will be inserted into. Furthermore, the user must download the missing UTXO Set Merkle tree paths, which are necessary in order to compute the Merkle tree root which is in turn used to verify the downloaded shards. It is important to note, that the user will request the shards of the \textbf{previous} block's UTXO Set in order to validate the current block. Given the fact that the user has already downloaded and verified the \textbf{block headers} for the blocks up to height $H$ and given a block of height $H+1$, the process of validating the block of height $H+1$ is described algorithmically below:
\begin{enumerate}
	\item \label{bvstep1} By looking at the transaction inputs' TXID in the transactions of the block of height $H+1$, the necessary shards which contain the corresponding Unspent Transaction Outputs can be determined using the first $k$ TXID bits, where $k$ is the index bits parameter.
	
	\item \label{bvstep2} By looking at the TXIDs of the transactions of the block of height $H+1$, the necessary shards which the new Unspent Transaction Outputs will be inserted into, can be determined by the first $k$ TXID bits.
	
	\item \label{bvstep3} The needed shards determined in steps \ref{bvstep1} and \ref{bvstep2} can be combined into a set of unique shards that need to be downloaded. Let these needed shards be a set of integers, $S$, of which integers are the shards' indices.
	
	\item \label{bvstep4} Using the set $S$, the missing UTXO Set Merkle tree hashes (tree nodes) required to compute the Merkle tree root can be determined. These hashes are necessary in order to verify the shards that will be downloaded later. The naive approach would be to download all the Merkle tree leaves (shard hashes) and compute the Merkle tree root. But since some of the shards will be downloaded and their hashes \textbf{must} be computed locally, there is no need to download the hashes themselves. Therefore, the following algorithm is presented to download the minimum number of missing hashes in order to compute the Merkle tree root:
	\begin{itemize}
		\item Initialize two empty sets, $P$ and $M$.
		
		\item For each element $i$ in the set $S$, add the element $(i,l,r)$ in the set $P$. The $l$ and $r$ are markers which symbolize the presence of left and right children respectively. If both children are present the hash can be calculated. Since $i$ is a shard index, the hash can be calculated directly from the shard's data.
	
		\item Repeat this step until termination. Remove all elements $x$ from $P$ and for each one of these elements:
		\begin{itemize}
			\item If $x=(p,l,r)$ and $p\equiv 0$ \textbf{then terminate and output $M$}.
			
			\item Else if $x=(p,l,-)$ and $p\equiv 0$ then add the element $m=p*2+2$ into set $M$ and \textbf{terminate and output $M$}.
			
			\item Else if $x=(p,-,r)$ and $p\equiv 0$ then add the element $m=p*2+1$ into set $M$ and \textbf{terminate and output $M$}.
			
			\item Else if $x \equiv (p,l,-)$, add the element $m=p*2+2$ into set $M$.
			
			\item Else if $x \equiv (p,-,r)$, add the element $m=p*2+1$ into set $M$.
			
			\item Compute the parent node $\hat{p}=\lfloor \frac{p-1}{2} \rfloor$. Add $\hat{p}$ into set $P$ using the following notation; if $2*\hat{p}+1 \equiv p$ add $(\hat{p},l,-)$, else add $(\hat{p},-,r)$. If $(\hat{p},l,-)$ or $(\hat{p},-,r)$ already exists into $P$, simply substitute the $-$ marker with the appropriate $l$ or $r$ marker.
		\end{itemize}
	\end{itemize}
	
	\item \label{bvstep5} Download the shards of which indices are denoted in set $S$ from step \ref{bvstep3}. Download the missing Merkle tree hashes of which node indices are denoted in set $M$ from step \ref{bvstep4}.
	
	\item Verify the downloaded shards by computing their hashes and using the downloaded missing Merkle tree hashes to compute the Merkle tree root. Compare the Merkle tree root to the UTXO Set Merkle tree root of block of height $H$. If the roots match, the shards have been downloaded correctly.
	
	\item Use the downloaded shards to validate the block of height $H+1$, as described in Section~\ref{fullblockverification}.
	
	\item Update the downloaded shards by removing the now ``spent'' Unspent Transaction Outputs and adding the new Unspent Transaction Outputs, as described in Section~\ref{fullblockverification}.
	
	\item Compute the hashes of the now modified downloaded shards and compute the new Merkle tree root. Finalize the validation of the block of height $H+1$ by comparing the new UTXO Set Merkle tree root to the one in the block of height $H+1$.
\end{enumerate}


\paragraph{Measuring the Block Verification Bandwidth} The bandwidth for the block verification is done as follows:
\begin{itemize}
	\item The bandwidth includes the cumulative size of the downloaded shards in step ~\ref{bvstep5} of the \textbf{Block Verification} algorithm.
	\item The bandwidth includes the cumulative size of the downloaded hashes in step ~\ref{bvstep5} of the \textbf{Block Verification} algorithm.
\end{itemize}

Once again, the bandwidth has been measured for every UTXO Set of block heights up to 520,000 and for the shard index bits values of the set~\ref{eq:bitsrange}.

\paragraph{Block Verification Bandwidth Measurements} The Block Verification Bandwidth measurements can be seen in Figure~\ref{fig:blockfullver}. About the Figure:
\begin{itemize}
	\item For index bits values from 0 to 8, practically the entire UTXO Set had to be downloaded. This shows almost, if not all, the shards were involved in order to validate the block. This happens because the number of shards is low compared to the number of Unspent Transaction Outputs in a block.
	
	\item When the index bits value is 19, the block verification bandwidth reaches up to 32MB.
	
	\item When the index bits take its maximum value of 26, the block verification bandwidth is optimally minimum, at just around 2 to 4MB.
\end{itemize}

\begin{figure}[t]
\centering
\includegraphics[]{blockFullVer.pdf}
\caption{Bandwidth (bytes) required to perform a full block verification and apply all Bitcoin rules in order to validate the block. The bandwidth is measured by adding up the sizes of the shards required to validate the block and the Merkle tree hashes (nodes) required to verify the shards. The figure includes graphs of various values for the \textbf{index bits parameter}.}
\label{fig:blockfullver}
\end{figure}


\section{Optimal Parameters}

\paragraph{UTXO Set Size vs Block Verification Bandwidth} In order to choose an optimal parameter for the index bits value and thus the shard size, the UTXO Set Size (Section~\ref{evaluationutxosetsize}) and the Block Verification Bandwidth (Section~\ref{evaluationblockverificationbandwidth}) have to be taken first into consideration. The Average Shard Size (Section~\ref{evaluationaverageshardsize}) has to be considered only in special cases where fewer or more shards are needed relatively to the shards needed in the Block Verification. Therefore, the Average Shard Size has a secondary priority when it comes to the optimal index bits value.

As analysed in Section~\ref{evaluationutxosetsize}, the UTXO Set size is optimal when the UTXO Set Merkle Tree and the Shards' metadata overhead is small. That happens when the index bits value takes its minimum value of 0 bits.

On the contrary, as analysed in Section~\ref{evaluationblockverificationbandwidth}, the block verification bandwidth is optimal when the average shard size is minimum or equivalently when the number of shards is maximum. That happens when the index bits value takes its maximum value of 26 bits.

\paragraph{Maximum Bitcoin Message Size} A parameter that will be taken into considaration for the optimal index bits value choice, is the Maximum Bitcoin Message Size. The maximum payload size for a Bitcoin message is 32MB, as documented in the Bitcoin Protocol~\cite{maxbitcoinmessagesize}.

It would be more efficient, as far as communication overhead between nodes in the Bitcoin network is concerned, if the block verification could be done in a single Bitcoin message. This means that the block verification bandwidth should be less than 32 MB. This requirement implies that we should partition the UTXO Set into at least $2^{19}$ shards or equivalently the index bits value should be at least 19 bits, a value extracted from Figure~\ref{fig:blockfullver}.

\paragraph{UTXO Set Merkle Tree Size Overhead} While the shards' metadata overhead is of considerable size for large index bits values, as shown in Section~\ref{evaluationutxosetsize}, it is not as much as the UTXO Set Merkle tree overhead for large index bits values. In order to set a limit for the Merkle tree size, the cumulative size of the blocks' headers is addressed. It is assumed that a Bitcoin node of minimal resources can store all the block headers or at least handle the computations for the block headers validation. Since the block header is only 80 bytes long (Section~\ref{howbitcoinworkstheblockchain}), all the block headers for a total of 520,000 blocks take about 40MB. Therefore, the target for the Merkle tree size is set to be around 40MB of size, because it is assumed that Bitcoin nodes with low resources will either be able to store or at least compute\footnote{Both the block verification and the Merkle tree root calculation can be done using  intermediate results in their computations. This means that the device that computes these tasks, does not need to have the memory capacity or storage capacity required by the entirety of the data it is computing.} tasks of data size relatively close to 40MB of size. Therefore, the index bits value is set to be at most 21 bits, which accounts for a Merkle tree size of about 128MB (equation~\ref{eq:merkletreesize}) and a UTXO Set size near the optimal value, as seen in Figure~\ref{fig:utxosetsizes}.


\paragraph{Optimal Index Bits Value} The final candidates of index bits values are down to 19, 20 and 21. Their measurements can be seen in Figures~\ref{fig:optutxosetsizes},~\ref{fig:optblockfullver} and~\ref{fig:optavgshardsize}.

Right away, the index bits value of 19 requires a bandwidth too close to the Bitcoin message limit of 32MB, so it is discarded. 

The index bits value of 20 produces a UTXO Set Merkle tree of 64MB of size, which is closer to the 40MB target, than the index bits value of 21, which as mentioned earlier, it produces a UTXO Set Merkle tree of size of 128MB.  

The index bits value of 21 offers a block verification bandwidth size well below 16MB, which is half the message limit size, and therefore produces less traffic in the Bitcoin network. It also produces an average shard size of just over 1MB.

This evaluation recommends the following index bits values as the optimal values that should be used for indexing the Unspent Transaction Outputs into the UTXO Set shards.
\begin{itemize}
	\item The recommended index bits value is \textbf{21 bits}.
	
	\item As an alternative, the index bits value of \textbf{20 bits} is also acceptable and within the targets of this evaluation.
\end{itemize}

\begin{figure}[t]
\centering
\includegraphics[]{optBlockFullVer.pdf}
\caption{Block Verification Bandwidth for optimal index bits values.}
\label{fig:optblockfullver}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[]{optUtxosetSizes.pdf}
\caption{UTXO Set Sizes (UTXO Set Merkle tree included) for optimal index bits values.}
\label{fig:optutxosetsizes}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[]{optShardAvgSize.pdf}
\caption{Average Shard Sizes for optimal index bits values.}
\label{fig:optavgshardsize}
\end{figure}