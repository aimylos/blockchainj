#!/bin/bash

latexfile=papergreek.tex

mkdir -p build
pdflatex -output-directory=build -interaction=errorstopmode -halt-on-error $latexfile && \
pdflatex -output-directory=build -interaction=errorstopmode -halt-on-error $latexfile