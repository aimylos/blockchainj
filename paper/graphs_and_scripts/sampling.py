import sys

samplingRate = 100

#Set sampling rate
if(len(sys.argv) == 2):
	samplingRate = int(sys.argv[1])


with sys.stdin as inf:
	with sys.stdout as outf:
		line = inf.readline()
		counter = 0
		while line:
			if(counter%samplingRate == 0):
				outf.write(line)
			line = inf.readline()
			counter += 1