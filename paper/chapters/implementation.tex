%Based on the proposal in Section~\ref{blockchainstatesolution}, an implementation is now presented in detail.
In order to see the solution proposed in Chapter~\ref{blockchainstatesolution} in action, a version of the proposal has been implemented and documented in this chapter.

\section{Guidelines} \label{dietcoinimplementation}
To implement the solution proposed in Chapter~\ref{blockchainstatesolution}, a detailed UTXO set format and data structure is necessary. The nodes in the network will have to parse and produce the UTXO set in that format so it has to be efficient but also complete with regard to the data it is going to provide. Therefore, a few guidelines are denoted to ensure that the data included in the UTXO set data structure are sufficient for performing a full block verification, including the transaction list validation.

It is reminded that the UTXO set of some height $H$ consists of all unspent transaction outputs up to the point the block of height $H$ has been successfully added to the blockchain and the block's transactions have been applied to the UTXO set as well. The proposal suggests splitting the UTXO set into shards, using those shards to create a Merkle tree and store its root into the Bitcoin block. This implementation will focus on using that extra information about the UTXO set to provide the user with a way to fully and securely verify the redeeming of not-yet-spent transaction outputs. Specifically the following statements will be used as guidelines to ensure the desirable result:

\begin{enumerate}[label=\textbf{S. \arabic*}] %,ref={S. \arabic*}]
	\item \label{statement1} The extra UTXO set information provided by a block of height $H$ can be used to fully verify the block's transactions at height $H+1$ provided that the UTXO set has not been built locally but access to it at height $H$ is possible. The level of trust of this verification is as high as the level of trust the SPV (Simplified Payment Verification, Section~\ref{buildingutxosetspvmethod}) method provides for verifying block headers.	

	\small{This is true, because unless a node builds the UTXO set itself, it cannot be sure of its integrity. Despite of that, using the  additional UTXO set information provided in the block, the node can verify the UTXO set's data when it receives it. Nevertheless, the strength of a chain is its weakest link and in this case the weakest link is the way the UTXO set's cryptographic digital fingerprint is obtained, which is the block itself.}
	%In the Dietcoin paper \cite{dietcoinpaper}, a method for increasing the SPV method security by using Dietcoin is mentioned. In that case, the node can adjust a parameter to stochastically increase its security, but it can not deterministically reach the same secuirity level of full nodes.}

	\item \label{statement2} Corollary of Statement~\ref{statement1}. The extra UTXO set information provided by a block of height $H$ can be used to fully verify the redeeming of a transaction output that exists in the UTXO set of height $H$, provided that access to the UTXO set of height $H$ is possible. The level of trust that a transction output exists legally in the UTXO set is as high as the level of trust the SPV method provides for verifing block headers.	

	\small{This statement describes the action of a node that has an interest in some transaction $T$ and wants to validate it. If the transaction $T$ has been included in some block of height $\hat{H} = H + K$ where $K>0$ and relatively small or has not yet been included in a block and the node has access to the UTXO set of height $H$, then it is possible for that node to obtain the necessary Unspent Transaction Outputs to validate the transaction $T$. The relatively small $K$ parameter, implies that if $K>1$, then the node will have to apply the intermediate blocks to its UTXO set of height $H$ in order to build the UTXO set of height $H+K-1$.} 

	\item \label{statement3} The extra UTXO set information provided by a block of height $H$ \textbf{cannot} be used to fully verify a transaction output that is in the UTXO set of height $H$. The level of trust that a transaction output exists legitimately in the UTXO set is as high as the level of trust the SPV method provides for verifying block headers.

	\small{This statement implies that if a node wishes to perform a validation following the Bitcoin Protocol rules of an Unspent Transaction Output that exists in the UTXO set of height $H$, it has to recursively backtrack that transaction's origins until it reaches the transaction of the Genesis block. In other words, there is no way to build the UTXO set using the extra UTXO set information provided by the block and achieve the same level of security with a node that downloaded and processed the whole blockchain to build its UTXO set.}	
\end{enumerate}

It is noted that: 

\begin{itemize}
	\item By fully, it is meant that the data can be verified and the Bitcoin protocol rules can be applied.
	\item By redeeming, it is meant that, provided a transaction input the transaction script verification can be done.
	\item By exists, it is meant that not only the transaction output can be found in the UTXO set at that height $H$, but it is also possible to obtain its data securely.
	\item By access to the UTXO set, it is meant that data, such as the UTXO set Merkle tree and UTXO set shards, can be obtained via the network securely.
\end{itemize}


\section{Unspent Transaction Outputs}

\subsection{Transaction Data Format} \label{transactiondataformat}
As mentioned in Section~\ref{bitcointransactions} a transaction contains of one or more transaction inputs and one or more transaction outputs. The UTXO set, though, only holds transaction outputs. The transaction inputs, once the block has been successfully appended to the blockchain, are not used anymore and therefore are discarded and not included in the UTXO set. As a result, not all the data from the transaction are needed, just the ``unspent'' transaction outputs.

\paragraph{Raw Transaction Data} The raw transaction data~\cite{bitcoinrawtransactionref} can be found in tables~\ref{tab:rawtransaction}~and~\ref{tab:rawtransactionoutput}. The transaction input format, txIn, has been skipped as it is not useful in any way right now. But there is one element of the transaction input that is interesting and that is the \textbf{outpoint}, (see Table~\ref{tab:rawoutpoint}). An outpoint is included in each transaction input to refer to a specific transaction output that the transaction input tries to spent. In other words, each transaction output can be uniquely referred to by its outpoint.

\begin{table}[h!]
	\begin{center}
    \caption{Raw Transaction Format}
    \label{tab:rawtransaction}
    \begin{tabular}{c|c|c|p{4cm}}
    	\textbf{Bytes} & \textbf{Name} & \textbf{Datatype} & \textbf{Description} \\
      	\hline
	  	4 & version & uint32\_t & Transaction version number \\
		\hline  
	  	1+ & tx\_in count & compactSize uint & Number of inputs in this transaction. \\
		\hline	  	
	  	36+ &tx\_in & txIn & Transaction Inputs. \\
	  	\hline
  	  	1+ & tx\_out count & compactSize uint & Number of outputs in this transaction. \\
  	  	\hline
  	  	9+ & tx\_out & txOut & Transaction Outputs. \\
  	  	\hline
  	  	4 & lock\_time & uint32\_t & A unix epoch time or block number. See protocol for more information. \\
  	\end{tabular}
  	\end{center}
\end{table}

\begin{table}[h!]
  	\begin{center}
    \caption{Transaction Output}
    \label{tab:rawtransactionoutput}
    \begin{tabular}{c|c|c|p{4cm}}
    	\textbf{Bytes} & \textbf{Name} & \textbf{Datatype} & \textbf{Description} \\
      	\hline
	  	8 & value & int64\_t & Number of satoshis assigned to this output. Must follow protocol rules. \\
      	\hline  	  	
  	  	1+ & pk\_script bytes & compactSize uint & Number of bytes in the pubkey script. Maximum is 10,000 bytes. \\
      	\hline  	  	
  	  	?+ & pk\_script & char[] & The pubkey script. \\
    \end{tabular}
  	\end{center}
\end{table}

\begin{table}[h!]
  	\begin{center}
    \caption{Outpoint}
    \label{tab:rawoutpoint}
    \begin{tabular}{c|c|c|p{5cm}}
      	\textbf{Bytes} & \textbf{Name} & \textbf{Datatype} & \textbf{Description} \\
      	\hline
	  	32 & hash & char[32] & The TXID of the transaction this outpoint points to. \\
      	\hline  	  	
  	  	4 & index & uint32\_t & The output index (outIndex) of the transaction this outpoint points to. \\
    \end{tabular}
  	\end{center}
\end{table}

\paragraph{About table~\ref{tab:rawtransaction}}
\begin{itemize}
	\item The transaction $\langle$version$\rangle$ is used to refer to which Bitcoin Protocol version rules need to be applied in order to validate it.
	\item The $\langle$compactSize uint$\rangle$ data type, \cite{compactsizeuintref}, is a variable length integer. It can be 1, 3, 5 or 9 bytes of size. For example, for numbers from 0 to 252 the $\langle$compactSize uint$\rangle$ only uses 1 byte to represent them, while for numbers from 253 to 65535 (0xFFFF) it uses 3 bytes.
	\item The $\langle$lock\_time$\rangle$ is a value that indicates the earliest time or earliest block when that transaction may be added to the block chain. If the value is less than 500 million, then the $\langle$lock\_time$\rangle$ is parsed as the block height and the transaction can be added to any block which has this height or higher. If the value is greater than or equal to 500 million, then the $\langle$lock\_time$\rangle$ is parsed using the Unix epoch time format and the transaction can be added to any block whose block time is greater than the $\langle$lock\_time$\rangle$.
\end{itemize}


\subsection{Stand-alone Transaction Output Data Format}

\paragraph{``Spent'' Transactions Outputs} For a UTXO set of some height $H$ and a specific transaction $T$, the transaction outputs of $T$ may \textbf{not} all be ``spent'' or ``unspent''. In fact, while that transaction $T$ is not fully ``spent''\footnote{For some height $H$, a fully spent transaction is a transaction that all its transaction outputs have been redeemed until that height $H$.}, there are one or more transaction outputs that are still ``unspent'' and need to be included in the UTXO set of height $H$. But, the transaction outputs that have been ``spent'' are no longer needed and can be discarded from the UTXO set.

\paragraph{Discarding the Transaction's Unutilized Data} As a result of discarding the transaction inputs (as mentioned in Section~\ref{transactiondataformat}) and the ``spent'' transaction outputs, a transaction's data that remain in the UTXO set are not cryptographically verifiable by the transaction's TXID. This happens because, as mentioned in Figure~\ref{fig:bitcoindetailedtransaction} from Section~\ref{bitcointransactionstransactions} a transaction's TXID is created by cryptographically hashing the transaction's data from table~\ref{tab:rawtransaction}. If all the data is not available, the TXID cannot be calculated. It is reminded that a transaction input that tries to spend a transaction output, will refer to the transaction output by its outpoint, table~\ref{tab:rawoutpoint}. The outpoint consists of the transaction output's TXID and index. Therefore, the TXID (and the index) will have to be included alongside with the transaction output.

It is important to point out that the transaction outputs that remain in the UTXO set do not require to be cryptographically verifiable by their TXID. Because even if they were (e.g. all the transaction data were included in the UTXO set), the existence of the transaction itself in the UTXO set would have to be cryptographically verifiable. Since this is the goal of this proposal, the data of the fragmented transactions are directly cryptographically bound to the UTXO set.

\paragraph{Transaction Output Metadata} Nevertheless, it becomes clear that the transaction output's data in table~\ref{tab:rawtransactionoutput} are insufficient if included in the UTXO set. Already, the TXID and index need to be included alongside with the transaction output. Not only that, but due to Bitcoin protocol rules, such as ``The coinbase\footnote{\label{coinbasefootnote1}The coinbase transaction is the first transaction in a block and belongs to the miner. Its transaction outputs can only be spent after a 100-block cooldown period. That means that for a  coinbase transaction of height $H$, its transaction outputs can only be spent by transactions that are included in a block of height $H+100$.} transaction can only be spent after a 100-block cooldown period'' where the coinbase's height is required, more metadata need to be included together with the transaction output.

Therefore, in order to proceed, it is useful to denote the necessary data that define a transaction output in a way that it can be used as a stand-alone piece of information to validate a corresponding future transaction input. The following comments will help create that denotation which can be seen in table~\ref{tab:standalonetransactionoutput}.

\paragraph{About Table~\ref{tab:standalonetransactionoutput}:}

\begin{itemize}
	\item The $\langle$lock\_time$\rangle$ (table \ref{tab:rawtransaction}) is only required when the transaction is added to a block. While it is necessary to make sure that the transaction output is indeed legitimate, it is not used to redeem it. Not including the lock\_time does not go against Statements \ref{statement1} and \ref{statement3}.
	\item The $\langle$version$\rangle$ may not seem necessary at first, since the current Bitcoin protocol treats all transactions as version 1. But transactions are allowed to have higher version numbers that special software may use for its consensus rules during the redeeming of the transaction outputs.
	\item A $\langle$coinbase$\rangle$ metadata value is needed when redeeming the coinbase\ref{coinbasefootnote1} transaction output.
	\item A $\langle$height$\rangle$ metadata value is needed when used with the $\langle$coinbase$\rangle$ value.
\end{itemize}

\begin{table}[p]
  	\begin{center}
    \caption{Stand-alone Transaction Output Data}
    \label{tab:standalonetransactionoutput}
    \begin{tabular}{c|c|c|p{4cm}}
      	\textbf{Bytes} & \textbf{Name} & \textbf{Datatype} & \textbf{Description} \\
      	\hline
  	  	32 & TXID & char[32] & The TXID of the transaction this outpoint points to. \\
		\hline  	  	
  	  	4 & version & uint32\_t & Transaction version. \\
		\hline
  	  	4 & outIndex & uint32\_t & The output index (outIndex) of the transaction this outpoint points to. \\
		\hline
	  	8 & value & int64\_t & Number of shatoshis assigned to this output. Must follow protocol rules. \\
		\hline
  	  	1+ & pk\_script bytes & compactSize uint & Number of bytes in the pubkey script. Maximum is 10,000 bytes. \\
		\hline
  	  	?+ & pk\_script & char[] & The pubkey script. \\
      	\hline
      	\multicolumn{4}{c}{\textbf{Metadata}} \\
	  	\hline
	  	1 & coinbase & uint8\_t & 0x01 if it is a coinbase output, 0x00 else. \\
		\hline
	  	4 & height & uint32\_t & The height of the block that contains this transaction output. \\
    \end{tabular}
    \end{center}  	
\end{table}

\pagebreak[0]
\newpage

\subsection{Unspent Transaction Outputs Data Format}

Due to the increasing size of the UTXO set, Figure~\ref{fig:utxosetsize}, it is important to eliminate redundant data into the UTXO set. With that in mind, by examining the stand-alone transaction output in table~\ref{tab:standalonetransactionoutput} the following comments can be made:

\begin{itemize}
	\item When more than one transaction outputs share the same TXID, storing the TXID multiple times is redundant.
	\item The $\langle$height$\rangle$ metadata value is the same for all transaction outputs sharing the same TXID. Storing it mutliple times is redundant. The $\langle$height$\rangle$ metadata value is also the same for all transaction outputs sharing the same block, but the ordering of the UTXO set is done based on the TXID value. That kind of ordering would require the use of pointers to link the $\langle$height$\rangle$ value to the corresponding transaction outputs, which would undermine the efforts of minimizing the UTXO set size in the first place.
	\item Transaction outputs that share the same TXID have the same $\langle$version$\rangle$, so storing the $\langle$version$\rangle$ multiple times is redundant.
\end{itemize}

Based on these comments, the final data format for a set of ``unspent'' transaction outputs that share the same TXID is shown in Table~\ref{tab:unspenttransactionoutputs}. That data format will be used to include the transaction outputs in the UTXO set.

\begin{table}[p]
  	\begin{center}
    \caption{Unspent Transaction Outputs Format (Common TXID)}
    \label{tab:unspenttransactionoutputs}
    \begin{tabular}{c|p{3cm}|p{2.2cm}|p{5cm}}
      	\textbf{Bytes} & \textbf{Name} & \textbf{Datatype} & \textbf{Description} \\
      	\hline
	  	32 & TXID & char[32] & The TXID of the following transaction outputs. \\
		\hline
	  	4 & version & uint32\_t & Transaction version. \\
      	\hline
      	\multicolumn{4}{c}{\textbf{Metadata}} \\
	  	\hline
	  	1 & coinbase & uint8\_t & 0x01 if it is a coinbase output, 0x00 else. \\
		\hline	  	
	  	4 & height & uint32\_t & The height of the block that contains this transaction output. \\
	  	\hline
      	\multicolumn{4}{c}{\textbf{End of Metadata}} \\
	  	\hline
	  	1+ & output\_count & compactSize uint & Number unspent transaction outputs with the same TXID. \\
		\hline
  	  	4 & outIndex & uint32\_t & The output index (outIndex) of the transaction this outpoint points to. \\
		\cdashline{3-4}
	  	8 & value & int64\_t & Number of shatoshis assigned to this output. Must follow protocol rules. \\
		\cdashline{3-4}
  	  	1+ & pk\_script bytes & compactSize uint & Number of bytes in the pubkey script. Maximum is 10,000 bytes. \\
		\cdashline{3-4}
  	  	?+ & pk\_script & char[] & The pubkey script. \\
		\hline   	  	
   	  	\dots & \dots & \dots & \dots \\
   	  	\hline
  	 	\dots & outIndex/value/ ps\_scirpt\_bytes/ pk\_script & \dots & Sorted by outIndex in ascending order. \\
    \end{tabular}
  \end{center}
\end{table}

\pagebreak[0]
\newpage

\section{UTXO Set Data Structure} \label{utxosetdatastructure}

This part of the implementation is probably the most important, as far as the proposal of Section~\ref{blockchainstatesolution} is concerned. Nevertheless, implementing a Merkle tree data structure is a straightforward process.

\paragraph{Algorithmic View} First of all, a detailed set of the steps that need to be followed in order to calculated the UTXO set fingerprint is listed bellow:

\begin{itemize}
	\item Define $N$ subsets, called shards, to split the UTXO set into. The number $N$ must be a power of 2.
		\begin{equation} \label{shardeq}
		\begin{aligned}
			\text{SHARD}_i = \left\lbrace \text{TXID} \in \text{UTXOSET} \text{ and } \text{TXID}_{\text{First } log_2N \text{ bits}} \equiv i_{\text{bits}} \right\rbrace , \\
		 \text{for } i=0...(N-1)
		\end{aligned}		
		\end{equation}
		
		In equation~\ref{shardeq}, the shards are defined as follows: 
		\begin{itemize}
			\item For transaction outputs that exist in the UTXO set and share a common TXID.
			\item Take the first $log_2N$ bits of the TXID.
			\item The decimal number of these bits is a number between $0$ and $N-1$.
			\item The decimal number of these bits is used to assign the transaction outputs to the corresponding shard $i$. 
		\end{itemize}
	
	\item Populate the shards with the corresponding transaction outputs using their TXID. It is reminded that the transaction outputs are in the table~\ref{tab:rawoutpoint} format.
	
	\item Sort the transaction outputs based on their TXID, in ascending order, for every shard.
	
	\item Compute the cryptographic hash of each shard using the SHA256\footnote{Perfoming the SHA256 hashing algorithm twice is set to be the standard hashing algorithm by the Bitcoin protocol. Therefore, it makes sense using this hashing algorithm for the UTXO set as well.} algorithm. Compute the cryptographic hash of each previously computed hash digest using the SHA256 algorithm, effectively performing a double SHA256 hashing of the shards' data.
	
	\item To build the Merkle tree use the list of hash digests that were calculated in the previous step, without altering the order.  Concatenate every two digests and calculate their double SHA256 hash. This process creates a new list, of which order must not be altered, which has half the cardinality of the original. By repeating the process until only one digest is left, the Merkle tree root is obtained. The intermediate lists of hash digests form the actual Merkle tree, figure~\ref{fig:merkleTreeChapterImp}.

	\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{merkleTreeUtxoset.png}
	\caption{UTXO Set Merkle Tree. }
	\label{fig:merkleTreeChapterImp}
	\end{figure}

\end{itemize}

\paragraph{Shard Size vs Merkle Tree Size} \label{shardsizevsmerkletreesize} Before moving on, it is important to keep in mind a crucial parameter, which is the shard size. The shard size is in direct correlation with the Merkle tree size. As mentioned in Section~\ref{merkletreeoverhead}, every time the shard size is halfed, the Merkle tree size is doubled. Therefore, being able to adjust the shard size is a desirable property. 

Unfortunately, Merkle trees are not as dynamic as conventional binary trees. More specifically, if the number of shards $N$ was to be doubled to $\hat{N}=2N$, the average shard size is expected to be halfed. The Merkle tree will be doubled in size by adding one more level at the bottom of the tree. That extra level though will cause a cryptographic inconsistency to all the levels above it, including of course the root, as shown in Figure~\ref{fig:merkletreeaddinglevel}. Therefore, the whole Merkle tree will need to be re-calculated and the Merkle tree root that was included in the block will have to be replaced, which is practically impossible once the block has been mined and appened to the blockchain.

\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{merkleTreeAddingLevel.png}
	\caption{Adding a level to the UTXO Set Merkle Tree. }
	\label{fig:merkletreeaddinglevel}
\end{figure}

Nevertheless, it is perfectly applicable to adjust the shard size and re-calculating the Merkle tree, if the block has not been mined yet. That would be the case if the Bitcoin protocol had to adjust to overcome future traffic and data volume challenges. For example, if the shard size was to be set at $\sim 1$MB per shard but the UTXO set size was doubled in size over time, the Bitcoin protocol could adjust by doubling up the number of shards and of course as a result, doubling the Merkle tree size.

\paragraph{UTXO Set Format}The Tables~\ref{tab:utxosetformat} and~\ref{tab:shardformat} denote the data format of the UTXO set and the shard. 

\paragraph{About the UTXO set format, Table~\ref{tab:utxosetformat}:}

\begin{itemize}
	\item The $\langle$Merkle\_Tree\_Root$\rangle$ field contains the digital fingerprint of the UTXO set that follows. This is the cryptographic hash digest that needs to be included into the block in order to bind the UTXO set to the block.
	
	\item The $\langle$Shard\textsubscript{0 to $N-1$}$\rangle$ fields contain the actual Unspent Transaction Outputs data formatted into shards, as denoted in Table~\ref{tab:shardformat}. The shards are sorted based on their index, in ascending order. 
\end{itemize}

\begin{table}[h!]
  	\begin{center}
    \caption{UTXO Set Format}
    \label{tab:utxosetformat}
    \begin{tabular}{c|c|p{3.4cm}|p{4cm}}
      	\textbf{Bytes} & \textbf{Name} & \textbf{Datatype} & \textbf{Description} \\
		\hline
	  	32 & Merkle\_Tree\_Root & char[32] & The UTXO Set Merkle Tree Root. \\
		\hline	  
	  	? & shard\textsubscript{0} & Shard & The shard format can found in table~\ref{tab:shardformat}. \\
	  	\hline
	  	\dots & \dots & \dots & Sorted by the shard index in ascending order. \\
		\hline	  	
	  	? & shard\textsubscript{$N-1$} & Shard & - \\
    \end{tabular}
  	\end{center}
\end{table}

\vspace{7mm}
\paragraph{About the Shard format, table~\ref{tab:shardformat}:}

\begin{itemize}
	\item The $\langle$version$\rangle$ field is used to indicate which Bitcoin protocol was used to produce the shard. The practice of including a $\langle$version$\rangle$ number in the data, such as the block version and the transaction version, is followed by the Bitcoin protocol. The importance of the version for the shard is primarily to indicate what the number of shards the UTXO set was split into is.

	\item The $\langle$index$\rangle$ field is used to indicate the shard's index. When the UTXO set is in the table~\ref{tab:utxosetformat} format, the shard index is implied and therefore it may seem like it is not needed. But when the shards are transferred individually, it is rather useful for the shard index to be included in the shard's data format. Nevertheless, the shard index value is indeed redundant if the necessary protocol rules are in place but a conscious trade-off between the $\langle$index$\rangle$ field and the extra protocol rules has been made so that the extra 8 bytes of the $\langle$index$\rangle$ field provide more flexibility for managing and transferring the shards.
	
	\item The $\langle$UTXOs\_count$\rangle$ field indicates the number of Unspent Transaction Outputs that follows. It's important to note that since the Unspent Transaction Outputs sharing a common TXID have been grouped, as seen in table~\ref{tab:unspenttransactionoutputs}, the $\langle$UTXOs\_count$\rangle$ value is actually the number of unique TXIDs in this shard.

	\item Finally, the Unspent Transaction Ouputs, $\langle$UTXOs$\rangle$, are included in the shard in the format seen in table~\ref{tab:unspenttransactionoutputs}. The $\langle$UTXOs$\rangle$ are sorted based on their TXID, in ascending order.
\end{itemize}

\begin{table}[h!]
  	\begin{center}
    \caption{Shard Format}
    \label{tab:shardformat}
    \begin{tabular}{c|c|p{3.6cm}|p{4cm}}
      	\textbf{Bytes} & \textbf{Name} & \textbf{Datatype} & \textbf{Description} \\
      	\hline
      	4 & version & uint32\_t & Shard's version of the Bitcoin protocol. Should be \texttt{0x00} for now. \\
      	\hline
      	8 & index & int64\_t & Shard's index. This field can take values from 0 to $N-1$, where $N$ is the number of shards. \\
      	\hline
   	  	1+ & UTXOs\_count & compactSize uint & Number of Unspent Transaction Outputs (UTXOs), as defined in table~\ref{tab:unspenttransactionoutputs}, in this shard. \\
		\hline	  	
	  	36+ & UTXOs\textsubscript{ 0} & Unspent Transction Outputs (Common TXID) & Unspent Transaction Outputs sharing a common TXID as defined in table~\ref{tab:unspenttransactionoutputs}. \\
		\hline	  	
	  	\dots & \dots & \dots & Sorted by TXID in ascending order. \\
		\hline	  	
	  	36+ & UTXOs\textsubscript{ UTXOs\_count $-1$} & Unspent Transction Outputs (Common TXID) & - \\
    \end{tabular}
  	\end{center}
\end{table}

\paragraph{Merkle Tree Data Structure} \label{merkletreedatastructure} The data structure used to maintain the Merkle tree need not be specified by the protocol. Since the only data needed by the protocol is the Merkle tree root, the way the Merkle tree is implemented is irrelevant. For the purposes of this implementation though, a minimal but efficient data structure for the Merkle tree is presented in table~\ref{tab:merkletreeformat}.

\paragraph{About Table~\ref{tab:merkletreeformat}:}

\begin{itemize}
	\item The first field, $N$, is the number of leaves (a.k.a. shards) of the Merkle tree. That number can be used to determine the size of the array that follows.
	
	\item The second field is the actual Merkle tree. Since this Merkle tree is a complete and balanced binary tree, it can be stored in a simple array of $2N-1$ cells (as many as the leaves and nodes of the tree), each of size of 32 bytes (as much as the SHA256 digest). The elements in the array can be accessed using the parent-child notation for tree-like data structures, as follows: 
	\begin{equation}
	\begin{aligned}
		&parent_i \text{, } i \in \lbrace 0,\dots ,N-2 \rbrace \text{ has two children, }\\
		&child_{2*i+1} \text{ and } child_{2*i+2}
	\end{aligned}
	\end{equation}
\end{itemize}

\begin{table}[h!]
  	\begin{center}
    \caption{Simple Merkle Tree Format}
    \label{tab:merkletreeformat}
    \begin{tabular}{c|c|p{3.4cm}|p{4cm}}
      	\textbf{Bytes} & \textbf{Name} & \textbf{Datatype} & \textbf{Description} \\
		\hline
      	8 & $N$ & int64\_t & $N$ is the number of shards. \\
      	\hline
	  	32 * (2$N$ - 1) & Merkle\_tree & char[32 * (2$N$ - 1)] & The UTXO set Merkle tree.
    \end{tabular}
  	\end{center}
\end{table}

\paragraph{Applying a block to the UTXO Set} At this point, it is possible to demonstrate how a block's transactions that are applied to the UTXO set would alter the UTXO set data and how would the Merkle tree be modified in order to compute the new Merkle tree root.

\paragraph{First it is assumed that:}
\begin{itemize}
	\item A valid UTXO set is built and locally stored in the Table~\ref{tab:utxosetformat} format. The height of the UTXO set is $H$.
	\item A valid block of height $H+1$ has just been obtained from the network. 
	\begin{itemize} 
		\item All the block's transactions have transaction inputs that match existing transaction outputs in the UTXO set and their transaction scripts execute successfully. 
		\item All the block's transactions have valid transaction outputs that can be added to the UTXO set.
	\end{itemize}
\end{itemize}

\paragraph{The process of appling the block of height $H+1$ to the UTXO set of height $H$ goes as follows:}
\begin{itemize}
	\item Remove all the newly spent transaction outputs from the UTXO set. Using the \textbf{OutPoint} (TXID and output index) of each of the transaction inputs that appear in the transactions in the block of height $H+1$, it is possible to determine which shards contain the corresponding transaction outputs. Once a shard is determined, the transaction outputs that have just been spent can be found and removed from it. Due to this process, at most all shards will be modified but if the number of shards $N$ is large enough, just a subset of the shards will be modified. Let that subset of shards be $S_1 \subseteq UTXO\_S$, where $UTXO\_S$ is the set of all shards.
	
	\small{It is possible for a block to have only one transaction, which is the coinbase transaction (the first transaction in the block). The coinbase transaction does not have any valid transaction inputs, only valid transaction outputs. Therefore, it is possible for a block to \textbf{not} spent any unspent transaction outputs from the UTXO set ($S_1 = \lbrace \emptyset \rbrace $).}
	
	\item Add all new unspent transaction outputs to the UTXO set. These new unspent transaction outputs come from the block's transactions. Using their TXID it is possible to determine which shards will these transaction outputs have to be included in. Once a shard is determined, the corresponding transaction outputs can be added in the shard at the right place so that the correct ordering is maintained. Due to this process, at least one\footnote{There must be at least one transaction output in the block. Even if there is one transaction in the block, that transaction must have at least one transction output.} shard will be modified. Let the subset of shards modified from this process be $S_2 \subseteq UTXO\_S \text{ and } S_2 \neq \lbrace \emptyset \rbrace$, where $UTXO\_S$ is the set of all shards.
	
	\item What remains, in order for the UTXO set of height $H$ to transition to the state of height $H+1$, is to re-calculate the Merkle tree root. To do that the two subsets $S_1$ and $S_2$ are used to identify the shards that have been modified. The subset $S_m = S_1 \cup S_2 \subseteq UTXO\_S$, is the set of all shards that have been modified and that their hash digest in the Merkle tree needs to be re-calculated. The shards that have not been modified, $UTXO\_S - S_m$, do not need their hash digest re-calculated, as it remains the same. Once the modified shards' hash digest have been re-calcuated, to calculate the new Merkle tree root efficiently, the following algorithm is provided which uses the \say{modified shards information} to its best advantage:
	\begin{itemize}
		\item[1] Start from the parents of the nodes of the tree that contain the shard's hash digest (a.k.a. parents of the leaves). The last parent of a leaf has the index of $(2N-1)-N-1 = N-2$ in the array for the Merkle tree data structure of table~\ref{tab:merkletreeformat}. So, starting from the index $i = N-2$.
		
		\item[2] If either the left child with index $2i+1$ or the right child with index $2i+2$ have a recently modified hash digest, then concatenate their hash digests and calculate the new hash digest for the current node $i$, effectively rendering this node $i$ as recently modified. Else, skip this step, leaving this node $i$'s hash digest as unmodified.
		
		\item[3] Decrement the index i by 1. If the index is negative continue to the next step, else go to step 2.
		
		\item[4] The new Merkle tree root can be retrieved at the position of index 0.
	\end{itemize}
	
	\item The block of height $H+1$ has now been applied to the UTXO set and the new UTXO set Merkle tree root has been re-calculated. The UTXO set's height is now $H+1$.
\end{itemize}


\section{Including the UTXO Set Fingerprint into the Block} \label{includingmerklerootintoblock}

The proposal in Section~\ref{blockchainstatesolution} requires that the UTXO set fingerpint be cryptographically bound to the corresponding block. This implementation proposes the following solution. For a block of height $H$:

\begin{itemize}
	\item Once the block of height $H$ has been created, but not mined yet, apply the block's transactions to the UTXO set of height $H-1$ and calculate the UTXO set Merkle tree root as described in Section~\ref{utxosetdatastructure}, effectively building the UTXO set of height $H$. Let $R$ be the 32 bytes long hash digest that is the UTXO set Merkle tree root.
	
	\item Place $R$ right after the 80 bytes of the block's header (The block's header can be seen in Figure~\ref{fig:bitcoinblockdetailed}). Perform a double SHA256 hash of the concatenation of the two merkle tree roots; the Transaction List Merkle tree root\footnote{The Transaction List Merkle tree that the Bitcoin block uses to cryptographically secure the transactions data should not be confused with the UTXO Set Merkle tree that has been proposed. The two Merkle trees are not linked in any way.} and the UTXO Set Merkle tree root $R$. Place the result in the block's header where the Transaction List Merkle tree root used to be. 
	
	\item ``Mine'' the block.
\end{itemize}

\paragraph{The first step} of this process is explained in detailed in Section~\ref{utxosetdatastructure} and its goal is to calculate the UTXO set Merkle tree root before the block is ``mined''. The fact that the block has not been mined yet means that there is no difficulty in modifying its header. Therefore, it is still possible to add information in the block. It is important to note though, the transactions of the block \textbf{cannot be modified}, because their TXIDs have already been included in the UTXO Set via the newly added Unspent Transaction Outputs.

\paragraph{The second step} is to include the UTXO set Merkle tree root in the block. Adding an extra field to the block's header is a difficult task for the Bitcoin Protocol and an unnecessary one. Therefore, the UTXO Set Merkle tree root $R$ has to be included elsewhere. 

A good idea would be to include a dummy transaction that contains $R$ and can be included in the Transaction List Merkle tree. This would not alter any existing transaction in the block's transaction list and $R$ would be cryptographically bound to the Transaction List Merkle tree root. But since a convention would be made anyway that, say the ``last transaction is the UTXO Set Merkle tree dummy transaction'', why not economize bytes and skip all the dummy transaction's metadata.

Therefore, the UTXO Set merkle tree root, $R$, is proposed to be included right after the block header's 80 bytes. This would create an extra field of 32 bytes between the block header and the transaction list. Then, to cryptographically bind $R$ into the block header, simply concatenate the Transaction List Merkle tree root and the UTXO Set Merkle tree root $R$ and compute the double SHA256 hash digest, which can then be included in the header where the Transaction List Merkle tree root would normally be. 

This process \textbf{does not} modify the block header's size, nor does it modify the transaction list or the Transaction List Merkle tree. In fact, it only increases the block size by 32 bytes with the fixed-position field $R$. It does though change the meaning of the Transaction List Merkle Tree Root field in the block header, to ``Merkle Tree Roots''.

\paragraph{Finally} the block can be ``mined''. It is reminded, from Section~\ref{howbitcoinworkstheblockchain}, that ``mining'' a block means to find a block header that once it is cryptographically hashed, the hash digest be arithmetically less than a protocol defined number. This process is computationally intensive and due to the chain effect of the blockchain, the block header's data are cryptographically buried in the blockchain at an exponential rate through time. 

The two Merkle tree roots, described above, are cryptographically bound together and included in the block header, prior to the block being ``mined''. Finally, the block, and by extension the block header, is ``mined'' and appended to the Blockchain. This is enough to prove that the UTXO set Merkle tree root has been cryptographically bound to the block itself.