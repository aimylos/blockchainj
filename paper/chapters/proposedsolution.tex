Given the issues discussed in Section~\ref{utxoproblem}, a solution is proposed that encodes a digest of the UTXO set into the block itself.

\section{Encoding the UTXO Set Into the Block} \label{encodingstateintoblock}

To overcome the issues described in Section~\ref{utxoproblem}, the solution proposed in this paper attempts to bind the UTXO set to the Blockchain. In order to achieve that, the solution aims to embed a cryptographic digital fingerprint of the UTXO set into the blocks themselves.

More specifically, given a valid UTXO set and the block that was last applied to it, the UTXO set's data are cryptographically hashed into a short hash digest. The hash digest can then be used as a digital fingerprint that can be included in the block\footnote{\label{hashinblockfootnote}A cryptographic hash is usually a short number of bytes (32bytes for the SHA256 algorithm). The hash could be included in the block's header or in a dummy transaction alongside the block's transactions.}. It is important to note that the block is first applied to the UTXO set as explained in Section~\ref{buildingutxoset} and then the UTXO set's fingerprint is calculated. This implies that the block has to be mined after the UTXO set's fingerprint is included in it.

\paragraph{Dietcoin Proposal} A solution has been proposed by a Bitcoin modification called Dietcoin~\cite{dietcoinpaper}. Note that the initial intent of Dietcoin was to offer security and functionality to lightweight Bitcoin nodes\footnote{Lightweight Bitcoin nodes can be considered divices such as smartphones or systems with low computational resources and especially low or limited bandwidth. These devices are not able to download the entire Bitcoin blockchain or even maintain the entire UTXO set locally. Until now, the most common solution for these devices has been the SPV method (Section~\ref{buildingutxosetspvmethod}).} but the fundamental property that Dietcoin offers, is to efficiently include a digital fingerprint of the UTXO set into the block.

The Dietcoin proposal requires the UTXO set to be structured in a way that it can be split into $ 2^k $ equal-sized subsets, called \textbf{shards}. The data in each of these shards, is then cryptographically hashed and the $ 2^k $ hashes are then used to build a fixed-size complete binary Merkle tree\footnote{A binary Merkle tree that has a number of leaves equal to a power of 2, is a complete binary tree.} (Section~\ref{merkletreesection}) as shown in Figure~\ref{fig:dietcoinsolutionsimple}. The Merkle tree root can then be included into the block.

\paragraph{As a result} Now that the UTXO set is hardcoded into the block, the nodes in the network can match their version of the UTXO set to the block's and effectively confirm that their computation of the UTXO set is done with the Bitcoin protocol version the miner used to create the block.

Note that including some kind of digital fingerprint of the state of the blockchain into its blocks is already done by other blockchain technologies, such as \cite[Ethereum]{ethereumsite}.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{dietcoinSolutionSimple.png}
\caption{A simplified example of splitting the UTXO Set into \textbf{shards} and building the \textbf{UTXO Set Merkle Tree}. The Merkle tree root is a cryptographic digest of the entire UTXO Set.}
\label{fig:dietcoinsolutionsimple}
\end{figure}

\section{Data Consensus and Integrity}

To calculate the UTXO set digital fingerprint the set has to be formatted in a specific data structure. This is necessary in order for all the nodes in the network to be able to calculate identical UTXO set fingerprints.

\paragraph{UTXO Set Order} Since the UTXO set consists of Transaction Outputs, it is necessary to reorder it. The order should follow the sorted order of the transaction outputs, in ascending order, primarily sorted on their TXID and secondarily on their output index, as seen in step 1 of Figure~\ref{fig:dietcoinsolutiondetailed}. As mentioned in Section~\ref{utxosetsection}, the order of the UTXO set is irrelevant to its data integrity, so reordering does not corrupt it.

\paragraph{UTXO Set Shards} Then the UTXO set can be split into $ 2^k $ equal-sized shards (subsets). That is easy if the first $ k $ bits of the transaction ouputs' TXID are used to index them into the correct shard, as shown in step 2 of Figure~\ref{fig:dietcoinsolutiondetailed}. Since the TXIDs are SHA256 cryptographic hashes, their bit distribution should be uniform and therefore it is expected that an equal number of transaction ouputs will be inserted into each shard. The size (in bytes) distribution of the transaction outputs themselves is uncorrelated to the TXIDs and therefore, the same transaction output size distribution is expected into each shard. As a result, the size of each shard is expected to be about equal.

\paragraph{UTXO Set Merkle Tree} Since the order of the transaction outputs in the shards has been set to be sorted in the way described above (splitting the UTXO set into shards did not change its ordering), every node in the network should have identical shards. By cryptographically hashing these shards and maintaining the order of the hashes, a set of hashes is obtained of which the cardinality is a power of 2 ($ 2^k $). The process of building the Merkle tree is now straightforward and the root of the Merkle tree is the digital fingerprint of the UTXO set (step 3 of Figure~\ref{fig:dietcoinsolutiondetailed}).

\paragraph{UTXO Set Successfully Bound to the Blockchain} The process described above does not corrupt the UTXO set in any way. Furthermore, if a single bit of the UTXO set is modified the Merkle tree root will completely and unpredictably change. That means, that by including the Merkle tree root into the block, the UTXO set's data is cryptographically \textbf{bound} to the block.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{dietcoinSolutionDetailed.png}
\caption{The process performed on the UTXO Set in order to compute the UTXO Set digest.}
\label{fig:dietcoinsolutiondetailed}
\end{figure}

\section{Usage and Functionality} \label{usageandfunctionalitysection}

Cryptographically binding the state of the blockchain into the block itself, provides the fundamental property of including the UTXO set data into the information that the Bitcoin distributed consesus agrees on. As as result, the issues described in Section~\ref{utxoproblem} can be overcome.

\subsection{UTXO Set Consent}
When a node peforms a block verification, in addition to the conventional verifications, it would have to verify that its UTXO set's Merkle tree root matches the one in that block. Accepting such a block as valid, would effectively be equivalent to giving consent to the Bitcoin protocol used to create the block. Such a property reinforces the community consensus of accepting protocol modifications, as described in Section~\ref{utxoproblem}.

The problem in the example with the two pairs of non-unique TXIDs presented in Section~\ref{utxoproblem} can now be resolved. The \cite[BIP30]{bip30} protocol modification forbids non-unique TXIDs that have not been fully spent to exist, with the exception of the ones that made the problem noticeble (in blocks of height 91,842 and 91,880). The Bitcoin community reacted to this rule by ignoring the first appearances of the TXIDs in blocks of height 91,722 and 91,812 and considering as valid the ones in the blocks of height 91,880 and 91,842 respectively. Of course, the consensus on that rule has yet to be proven, since the transactions in question have not yet been spent. 

By including the UTXO set's digital fingerprint into the block, it would force the network to consent to the rule. The UTXO set would be modified shortly after the BIP30 protocol modification has been introduced, as shown in figure~\ref{fig:nonuniquetxids}. If the network accepts the rule, it will have to accept the blocks with the modified UTXO set's digital fingerprint as well.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{nonuniquetxid.png}
\caption{The UTXO set is modified short after BIP30 is introduced.}
\label{fig:nonuniquetxids}
\end{figure}

\subsection{Bootstraping to the Bitcoin Network}
An advantage of including the UTXO set's fingerprint into the block is that the UTXO set is now cryptographically verifiable once downloaded by a node in the network. More specifically the way a node would be able to synchronize with the Bitcoin network efficiently and securely would be by using the SPV method (mentioned in Section~\ref{buildingutxosetspvmethod}) to securely acquire the blockchain's block headers. Once the headers are available and validated, the most current version of the UTXO set can be downloaded from the network and its fingerprint can be calculated and matched with the UTXO set fingerprint from the according block.

Once the node has a complete, recent and verified version of the UTXO set, it can validate newcoming blocks in order to append them to the blockchain, even though the node does not have the entire blockchain downloaded. For example, if the node was bootstrapping at a blockchain height of 525,000, it would have to download 40MB of data for the block headers and another 3GB of data for the UTXO set, while performing a relatively modest amount of computations. Compared to the size of the entire blockchain (170GB of data), using the method just described offers a relatively quick bootstrapping.

\subsection{Computational Overhead} \label{merkletreeoverhead}
Finally, it is also important to show the computational impact on full nodes and miners from having to maintain an ordered UTXO set and a fixed-sized Merkle tree.

\paragraph{Maintaining the Merkle Tree} The \textbf{fixed-sized} Merkle tree is easy to maintain and update as long as the size itself is relatively manageable. Taking into consideration the number of unspent transaction outputs in the UTXO set, Figure~\ref{fig:utxocount}, a Merkle tree with 8192 leaves  would account for 0.5MB\footnote{A complete binary tree that has $ n $ leaves, has $ 2n-1 $ nodes in total. Using the SHA256 cryptographic hash algorithm, every node would need 32 bytes of memory to store the hash. The final formula for the Merkle tree's size is $ (2n-1)\cdot 32 $ bytes.} of memory in order to store the tree and on average $ \frac{60 \cdot 10^6 utxo}{8192 shards} = 7324 $ unspent transaction outputs per shard. Both the shards and the Merkle tree would be computationally cheap to maintain. But, since the size of the Merkle tree doubles every time its leaves double, an extreme case of one unspent transaction output per shard, means the Merkle tree would have $ 2^{26} = 67,108,864 $ leaves, resulting in a need of 4GB of memory to store it. In any case, it is important that the computation required to calculate the Merkle tree root for every new block is within the capabilities of the average user.

\begin{figure}[t]
\centering
\includegraphics{utxoCount.pdf}
\caption{UTXO Set's Unspent Transaction Count.}
\label{fig:utxocount}
\end{figure}

\paragraph{Maintaining an Ordered UTXO Set} As far as maintaining an ordered UTXO set, there is nothing too computationally expensive there. In fact, the UTXO set's shards are mutually exclusive with respect to their data and can be accessed and modified independently. This means, shards can be sorted in parallel and at a lower computational cost.

\paragraph{Mine the Block} At last, as mentioned earlier, the miner will have to gather the block's transactions (Section~\ref{theblockchainincentivesandminers}), verify them and apply the modifications that the transactions caused to the UTXO set (Section~\ref{buildingutxoset}) and as the \textbf{extra step} calculate the new UTXO Merkle tree root. Then include that information in the block and ``mine'' the block (Section~\ref{theblockchainproofofwork}).