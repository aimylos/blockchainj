import sys

#parameters
delim = "\t"
movAvgLen = 1000

#Set mov avg len
if (len(sys.argv) == 2):
	movAvgLen = int(sys.argv[1])

#read file line by line
with sys.stdin as inf:
	fileInLines = inf.readlines()

#remove newline
strippedInLines = [s.rstrip() for s in fileInLines]

#parse lines
parsedLines = [[float(val) for val in s.split(delim)] for s in strippedInLines]

#input lines
inLines = parsedLines

#output lines
outLines = list()

#sums
sums = list()

#main loop
for i in range(len(inLines)):
	xval = int(inLines[i][0])
	inLine = inLines[i][1:]

	if (i == 0):
		outLine = [xval] + inLine
		sums = inLine

	elif (i<movAvgLen):
		sums = [(val1+val2) for val1, val2 in zip(inLine, sums)]
		outLine = [xval] + [(val)/(i+1) for val in sums]

	else:
		headOutLine = inLines[i - movAvgLen][1:]
		sums = [(val1+val2-val3) for val1, val2, val3 in zip(inLine, sums, headOutLine)]
		outLine = [xval] + [(val)/movAvgLen for val in sums]

	
	outLines.append(outLine)
	# print(outLines[-1])
	# raw_input("Pressanykey")



#write to output
with sys.stdout as outf:
	for outLine in outLines:
		outf.write(str(outLine[0]) + "\t")
		for val in outLine[1:]:
			outf.write(str(round(val,2)) + "\t")
		outf.write("\n")