import sys

inStr = ""

#get string
if(len(sys.argv) == 2):
	inStr = sys.argv[1]

outStr = ""
index = len(inStr) - 2
while(index >= 0):
	outStr += inStr[index] + inStr[index+1]
	index -= 2

print(outStr)