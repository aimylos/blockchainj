Starting from an initial state and transitioning to a new state for every block appened to it, the blockchain can be viewed as a state transition system.

\section{Unspent Transaction Output Set - UTXO set} \label{utxosetsection} 

\paragraph{UTXO Set as the State of the Blockchain} Being a ledger, the blockchain has at any given time a state. While the blockchain does record all the transactions that have been made in a chronological order, in practice, one usually cares about the current state of an unspent transaction output\footnote{It is reminded that an unspent transaction output is a transaction output that has not yet been claimed by a transaction input of a valid transaction in a valid block that has been successfully added to the blockchain.}. The state of a transaction output can be spent or unspent, has a BTC value assigned to it and has a script that describes how to spend it, as described in Chapter~\ref{bitcointransactionstransactions} and seen in figures~\ref{fig:bitcointransactiontree}~and~\ref{fig:bitcoindetailedtransaction}. After a valid block has been appended to the blockchain, some unspent transaction outputs will become spent transaction outputs and some new unspent transaction outputs will be created. \textbf{The state of the blockchain is the set of all Unspent Transaction Outputs (UTXO set) after a valid block has been appended to the blockchain}. That state does not change until the next block is appended.

All the unspent transaction outputs in the UTXO set are unique and can be identified by their TXID in combination with their output index. The data required to fully represent an unspent transaction output is the TXID, the output index, the BTC value and the output script. Due to the Bitcoin protocol though, it is also useful to store metadata, such as the block's hash and the block's height\footnote{The height of the block is the block's sequential number in the blockchain. The genesis (first) block has a height of 0, the second a height of 1, ect.} in which the unspent transaction output was included, to perform several rule verifications. Finally, the order of the unspent transaction outputs in the UTXO set is irrelevant.


\section{Building and Using the UTXO set} \label{buildingutxoset}

\paragraph{Empty UTXO Set} To better understand the importance of the UTXO set, it is helpful to explain how it is built. Starting from an empty blockchain, the UTXO set is also empty. Once the genesis (first) block has been created, the first (can be more than one) unspent transaction outputs are added to the UTXO set (figure~\ref{fig:utxosetbuild}).

\begin{figure}[t]
\includegraphics[width=0.9\textwidth]{utxoSetBuild.png}
\caption{An oversimplified example of the UTXO set as the blockchain progresses.}
\label{fig:utxosetbuild}
\end{figure}

\paragraph{SPV Block Verification}\label{buildingutxosetspvmethod} For every block after the genesis block, a Bitcoin node will most likely want to validate those blocks. In order to validate a block, both the block header and the transaction list (figure~\ref{fig:bitcoinblockdetailed}) must be valid. Validating the header is easy, computationally and bandwidth-wise cheap (only 40MB of data for the current 525,000 blocks) and the method that only validates block headers is called Simplified Payment Verification (SPV). The SPV method requires the user to trust the Bitcoin network to verify and validate the blocks' transactions and is unable to obtain any information about the UTXO set in a \textbf{trustless} way. For example, when using the SPV method, a query such as

{%
\centering
\say{give me all the unspent transaction outputs that contain the cryptographic address XXXXYYYY in their output scripts} \par
}
\noindent would be performed on blind trust towards the Bitcoin network. 

\paragraph{Full Block Verification}\label{fullblockverification} The second part of validating a block, is verifying and validating its transaction list. This action is performed by \textbf{``full nodes''}. Full nodes can be any Bitcoin user or miner and their main property is to operate in the Bitcoin network in a trustless way. In order to achieve that, the full node must download the entire blockchain (170GB of data for the current 525,000 blocks). While the block's header verification is easy, the full node must also verify the block's transactions which constitutes pretty much the 170GB of blockchain data.

When verifying the transactions in order to append a block to its blockchain, the full node will have to look at the transaction inputs of each transaction and make sure that the transaction outputs they try to ``spend'' are still ``unspent''. \textbf{Here is where the UTXO set comes into play}, by providing the information about the yet ``unspent'' transaction outputs. After verifying that all transaction inputs refer to ``unspent'' transaction outputs and that the transaction input scripts algorithmically match the corresponding transaction output scripts, the UTXO set can be updated by removing all the now ``spent'' transaction outputs and adding all the newly created ``unspent'' transaction outputs, as shown in figure~\ref{fig:utxosetbuild} for the blocks of height 1 and 2.

\paragraph{Bitcoin as a State Transition System} It is interesting to mention a more technical approach of appending a block to the blockchain and modifying the UTXO set as a result. The idea is to view Bitcoin as a State Transition System (figure~\ref{fig:bitcoinstatetransition}) where a state would transition to another state by applying a transition function that receives as input the current state and the new block:

\[ \texttt{APPLY(STATE, BLOCK) -> STATE' OR ERROR} \]

More information on this matter can be found in Ethereum's white paper~\cite[See: Bitcoin as a State Transition System]{ethereumwhitepaper}.

\begin{figure}[H]
\includegraphics[width=\textwidth]{bitcoinStateTransition.png}
\caption{Bitcoin as a State Transition System.}
\label{fig:bitcoinstatetransition}
\end{figure}


\section{UTXO Set vs Blockchain} \label{utxoproblem}

\paragraph{UTXO Set Challenge} Being the state of the blockchain, the UTXO set is an indispensable piece of information that needs to be computed and maintained accurately by the nodes in the network. Unfortunately, as important as the UTXO set might be, its data are \textbf{not bound} in any way with the blockchain itself, other than the algorithmic procedure to produce it, as described by the Bitcoin Protocol. The two main issues that derive from that lack of binding are:

\begin{itemize}
	\item The inability of the network to consent on the UTXO set's data itself in a secure and cryptographic manner.
	\item As a corollary, there is no other way to securely obtain the UTXO set, other than build it from scratch. In other words, there is no quick and secure way to fully bootstrap to the Bitcoin network.
\end{itemize}

\paragraph{UTXO Set Consent} The UTXO set is as mentioned before the state of blockchain and should be identical for all Bitcoin nodes. If it is not identical, that would mean that there would be at least two Bitcoin nodes with the exact same blockchain but with different versions of UTXO sets. In other words, one node may view some transaction outputs as spent while another may view them as unspent. Such a situation would contradict the hypothesis that the Bitcoin system offers some kind of solution for the Distributed Consensus Problem, as mentioned in Chapter~\ref{whatisbitcoin}.

The way Bitcoin ensures that the state of the blockchain will be identical for all Bitcoin nodes, is based on the assumption that everyone follows the same protocol rules. While this is in fact true for experienced Bitcoin users (and probably most Bitcoin users that hold significant positions in BTC) because they have a strong incentive to be in compliance with the network, it might not be true for all Bitcoin software that exists and that might be being developed. The changes and mostly the addition of rules to the Bitcoin protocol, that have been approved by community consensus, make it harder for all Bitcoin software\footnote{One of the most popular Bitcoin software is Bitcoin Core~\cite{bitcoincore}. It is vastly used as a point of reference for developing Bitcoin software.} to output identical UTXO sets. Furthermore, a modification in the Bitcoin protocol may not be accepted by the entire community and may lead to consensus inconsistencies in future transactions.

\paragraph{Example of Bitcoin Protocol Modification} For example, there are two instances in the Bitcoin blockchain that two transactions share the same TXID. The transactions in question are the following two pairs:

\begin{itemize}
	\item TXID: {\footnotesize e3bf3d07d4b0375638d5f1db5255fe07ba2c4cb067cd81b84ee974b6585fb468}
		\begin{itemize}
			\item First appearance in the block of height 91,722
			\item Second appearance in the block of height 91,880
		\end{itemize}
	\item TXID: {\footnotesize d5d27987d2a3dfc724e359870c6644b40e497bdc0589a033220fe15429d88599}
		\begin{itemize}
			\item First appearance in the block of height 91,812
			\item Second appearance in the block of height 91,842
		\end{itemize}
\end{itemize}

\noindent All four of these transactions where the coinbase transactions\footnote{As mentioned in Chapter~\ref{theblockchainincentivesandminers}, the first transaction in a Bitcoin block is called coinsbase transaction and its inputs are ignored. Its outputs are the block's rewards plus the sum of the transaction fees in the block.} in their respective blocks and therefore their inputs and by extent their input scripts were irrelevant and they could be any random string of characters. Since the first transaction in a block belongs to the miner, they are the ones that chose the input scripts to be identical to each other. That led to two Bitcoin protocol modifications, first \cite[BIP30]{bip30} and then \cite[BIP34]{bip34}, that forbids duplicate TXID's if one of them is not spent and requires miners to include the block's height into the input script of the first transaction in the block to ensure TXID uniqueness. 

While the \cite[BIP30]{bip30} and \cite[BIP34]{bip34} specifically document the actions need to be taken, the rules are not apparent in the blocks that follow. New, existing or even malicious Bitcoin software may not handle all of these and the new coming rules as they should. For example, if a software for full nodes were to keep these duplicate TXID transactions in its UTXO set, they would have no idea that these transactions are there, since they have never been spent until now. Differences like that in the UTXO set between nodes, goes against the principle of the Bitcoin blockchain that guarantees consensus throughout the network.

\paragraph{Bootstraping to the Bitcoin Network}
The size of the UTXO set is 3GB for the current 525,000 blocks, compared to the blockchain's size of 170GB. The difference in size is due to the fact that most transaction outputs have been spent. As a result, the time, the bandwidth and the computational resources that it takes for a full node to download and process the entire blockchain and output the most current version of the UTXO set, are already significant and will continue to increase as the Bitcoin blockchain grows. The size of the blockchain and the UTXO set can be seen in figures~\ref{fig:blockchainSize} and~\ref{fig:utxosetsize}. Both sizes grew exponentially and are expected to continue to grow at a linear rate, although that depends on Bitcoin's popularity. Nevertheless, the Bitcoin blockchain's volume of data is significant and after it has been processed, its output (the UTXO set) needs to be \textbf{identical} and \textbf{verifiable} by every node in the Bitcoin network.

%\begin{figure}[H]
%\centering
%\begin{minipage}{.5\textwidth}
%  \centering
%  \includegraphics{blockchainSize.pdf}
%  \caption{\scriptsize Uncompressed blockchain size.}
%  \label{fig:blockchainSize}
%\end{minipage}
%\begin{minipage}{.5\textwidth}
%  \centering
%  \includegraphics{utxosetSize.pdf}
%  \caption{\scriptsize Uncompressed UTXO set size.}  
%  \label{fig:utxosetsize}
%\end{minipage}
%\end{figure}

\begin{figure}[t]
	\centering
	\includegraphics{blockchainSize.pdf}
	\caption{Uncompressed blockchain size.}
  	\label{fig:blockchainSize}
\end{figure}

\begin{figure}[t]
	\centering
  	\includegraphics{utxosetSize.pdf}
  	\caption{Uncompressed UTXO set size.}  
  	\label{fig:utxosetsize}
\end{figure}
  