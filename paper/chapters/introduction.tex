As of November 2018, Bitcoin has been in circulation for almost 10 years. 
\begin{wrapfigure}[5]{r}{0.2\textwidth}
	\centering
	\includegraphics[width=0.1\textwidth, keepaspectratio]{bitcoinLogo.png}
	\caption{Bitcoin Logo.}
	\label{fig:bitcoinLogo}
\end{wrapfigure}
Bitcoin was the first cryptocurrency\footnote{Cryptocurrency~\cite{cryptocurrency} stands for cryptographic currency.} that used the blockchain technology and was completely decentralized. Ever since Bitcoin, there have been hundreds of cryptocurrencies created, based on the initial concept of blockchains that Bitcoin introduced. Some of the most popular cryptocurrencies can be seen in Figure~\ref{fig:cryptoLogo}.

\begin{figure}[t]
\centering
\includegraphics[width=1\textwidth]{cryptocurrenciesLogo.jpeg}
\caption{Just some of the most popular cryptocurrencies in circulation.}
\label{fig:cryptoLogo}
\end{figure}

\paragraph{Blockchain Technology} The blockchain consists of blocks of data called \textbf{blocks}, which are connected in an append-only list, as shown in Figure~\ref{fig:genericBlockchain}. The append-only property of this list is guaranteed by two cryptographic mechanisms:
\begin{itemize}
	\item First, every block appended to the blockchain is cryptographically linked to its previous block, which at the time is the last block in the blockchain. Once the new block is appended, it becomes the last block of the blockchain.
	
	\item Second, in order to create a block, a substantial computational effort has to be made to compute it, which can only be achieved through the collective effort of a vast network of computers. This network is public and anyone can join it with ease, thus creating a collective computational power which is hardly matched by any single party on their own.
\end{itemize}
Due to these two cryptographic mechanisms, the blockchain plays the role of a \textbf{public ledger}, which is  \textbf{immutable} and \textbf{identical} for everyone in the network.

Cryptocurrencies take advantage of the blockchain's properties to store a financial ledger of transactions in the blocks, thus creating a decentralized cryptographic currency system. Of course, the blockchain technology has not only been used to support cash systems, but innovative ideas such as the Ethereum Project\footnote{Ethereum Project: \url{https://www.ethereum.org/}} have emerged which instead of simple financial transactions, Ethereum offers the possibility of creating transactions that execute complex algorithmic procedures essentially creating what is known today as ``smart contracts''\footnote{A smart contract is a computer protocol intended to digitally facilitate, verify, or enforce the negotiation or performance of a contract. Smart contracts allow the performance of credible transactions without third parties. These transactions are trackable and irreversible. \url{https://en.wikipedia.org/wiki/Smart_contract}}.

\begin{figure}[t]
\centering
\includegraphics[width=1\textwidth]{genericBlockchain.png}
\caption{An oversimplified view of the Blockchain.}
\label{fig:genericBlockchain}
\end{figure}


\paragraph{Blockchain State} Since the blockchain's blocks are used to store transaction data, whatever those transactions may be and whether they interact with each other or not, they add meaningful information to the blockchain. The accumulated information added by each block of the blockchain, up to the last block appended, can be used to define the state of the blockchain at that time. For every new block appended to the blockchain, the state of the blockchain changes to a new one.

While cryptocurrencies like the Ethereum Project include a cryptographic fingerprint of their blockchain state in each block appended, 
\begin{wrapfigure}{r}{0.6\textwidth}
	\centering
	\includegraphics[width=0.6\textwidth, keepaspectratio]{blockchainSize.pdf}
	\caption{Blockchain size (bytes) over the years. It is pointed out that 1GB consists of $2^{30}$ bytes.}
	\label{fig:introBlockchainSize}
\end{wrapfigure}
the Bitcoin blockchain does not include any information about its state in the block itself. As a result, in order to calculate the most recent state of the Bitcoin blockchain, an extensive computational task has to be performed by processing each block from the first to the most recent one in chronological order. This task also includes downloading the entire blockchain, of which size has grown to surpass the 128GB and continues to grow, as seen in Figure~\ref{fig:introBlockchainSize}.

%\begin{figure}[t]
%\centering
%\includegraphics[width=1\textwidth]{blockchainSize.pdf}
%\caption{Blockchain size (bytes) over the years. It is pointed out that a GB consists of $2^30$ bytes.}
%\label{fig:introBlockchainSize}
%\end{figure}

\paragraph{Motivation} The goal of this report is to study the Bitcoin's blockchain state in detail and format it into a data-structure that can be used to efficiently compute a new, unquie and secure digital cryptographic fingerprint for that state, each time a block is appended. Furthermore, the cryptographic fingerprints of the state will have to be included into the corresponding blocks themselves in a secure and cryptographic manner, in order to take advantage of the blockchain's immutability of data. The concept is demonstrated in Figure~\ref{fig:genericblockchainstate}.

\begin{figure}[t]
\centering
\includegraphics[width=1\textwidth]{genericBlockchainState.png}
\caption{An oversimplified view of including a cryptographic fingerprint of the blockchain state into the blocks.}
\label{fig:genericblockchainstate}
\end{figure}

\paragraph{Structure of this Document} In this document the reader will find a detailed documentation of the Bitcoin system and the Bitcoin's blockchain state in Chapters~\ref{bitcoinsection} and~\ref{blockchainstatechapter} respectively. A proposed solution for computing and including a cryptographic fingerprint of the Blockchain state into the blocks is presented in Chapter~\ref{blockchainstatesolution} and a detailed implementation is documented in Chapter~\ref{technicaldetailssection}. Finally, an evaluation of the solution can be found in Chapter~\ref{evaluationsection}.